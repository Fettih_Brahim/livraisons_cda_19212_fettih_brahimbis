package service.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import persistence.bean.Chien;
import persistence.dao.ICatalogueDAO;
import service.ICatalogueService;
import service.dto.ChienDto;
import service.dto.mapper.ChienMapper;

@Service
@Transactional(propagation = Propagation.REQUIRED)
public class CatalogueService implements ICatalogueService {

	@Autowired
	ICatalogueDAO catalogueDAO;
	@Autowired
	ChienMapper chienMapper;

	@Override
	public ChienDto getChien(Integer noPuce) {
		return catalogueDAO.getChien(noPuce);
	}

	@Override
	public void deleteChien(Integer noPuce) {
		catalogueDAO.deleteChien(noPuce);
	}

	@Override
	public List<ChienDto> listeChiens() {
		return chienMapper.mapListToDto(catalogueDAO.listeChiens());
	}

	@Override
	public void updateChien(Chien p) {
		catalogueDAO.updateChien(p);
	}

	@Override
	public ChienDto addChien(ChienDto chienDto) {

//		final Chien chienDo = catalogueDAO.addChien(chienMapper.mapToEntity(chienDto));
//		
//		return chienMapper.mapToDto(chienDo);
		
		catalogueDAO.addChien(chienMapper.mapToEntity(chienDto));
		return chienDto;
	}
}
