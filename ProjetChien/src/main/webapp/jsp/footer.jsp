<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="org.apache.struts.action.ActionErrors"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>


<!--Footer-->
<footer class="page-footer text-center font-small mt-4 wow fadeIn">

	<!--Call to action-->
	<div class="pt-4">
		<h1 class="mb-4">
			<strong>CHIENS-GALAXIE</strong>
		</h1>
	</div>
	<!--/.Call to action-->

	<hr class="my-4">

	<!-- Social icons -->
	<div class="pt-4">
		<html:link href="lister.do" styleClass="white-text">
			<h2>L'Univers des Chiens</h2>
		</html:link>
	</div>
	<!-- Social icons -->

	<!--Copyright-->
	<div class="footer-copyright py-3">© 2020 Copyright: CDA 19212</div>
	<!--/.Copyright-->

</footer>
<!--/.Footer-->
