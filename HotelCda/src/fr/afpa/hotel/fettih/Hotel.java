package fr.afpa.hotel.fettih;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Hotel {

	private static String nomHotel = "Java";
	private static int nombreDeChambre = 20;
	private static List<Chambre> chambres = new ArrayList<Chambre>();

	public Hotel() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Hotel(String nomHotel, int nombreDeChambre, List<Chambre> chambres) {
		super();
		Hotel.nombreDeChambre = nombreDeChambre;
		Hotel.nomHotel = nomHotel;
		Hotel.chambres = chambres;
	}

	public static void chambreNonDisponible(Hotel nom) {
		int count = 20;
		for (Chambre chambre : Hotel.chambres) {
			if (chambre.isDisponible())
				count--;
		}
		System.out.println("Le nombre de chambres occup�es est de = " + count);
	}

	public static void chambreDisponible(Hotel nom) {
		int count = 0;
		for (Chambre chambre : Hotel.chambres) {
			if (chambre.isDisponible())
				count++;
		}
		System.out.println("Le nombre de chambres libres est de = " + count);
	}

	public static void premiereChambreDisponible(Hotel nom) {

		boolean nn = false;
		int a = 0;
		for (Chambre chambre : Hotel.chambres) {
			if (chambre.isDisponible() && nn == false) {
				nn = true;
				a = chambre.getNumeroDeChambre();
			}
			
			System.out.println(a);
		}
	}

	public static int premiereChambreDisponible() {

		boolean nn = false;
		int a = 0;
		for (Chambre chambre : chambres) {
			if (chambre.isDisponible() && nn == false) {
				nn = true;
				a = chambre.getNumeroDeChambre();
			}
		}

		return a;
	}

	public static void derniereChambreDisponible(Hotel nom) {

		Integer b = null;
		for (Chambre chambre : Hotel.chambres) {
			if (chambre.isDisponible()) {

				b = chambre.getNumeroDeChambre();

			}
		}

		System.out.println("La dern�re chambre vide est la num�ro " + b);
	}

	public int getNombreDeChambre() {
		return nombreDeChambre;
	}

	public void setNombreDeChambre(int nombreDeChambre) {
		Hotel.nombreDeChambre = nombreDeChambre;
	}

	public String getNomHotel() {
		return nomHotel;
	}

	public void setNomHotel(String nomHotel) {
		Hotel.nomHotel = nomHotel;
	}

	public static List<Chambre> getChambres() {
		return chambres;
	}

	public static void setChambres(List<Chambre> chambres) {
		Hotel.chambres = chambres;
	}

	@Override
	public String toString() {
		return "Hotel " + nomHotel + ", " + "Capacit� d'accueil = " + nombreDeChambre;

	}

	public static void affichage() {

		for (Chambre chambre : chambres) {

			System.out.println(chambre);

		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Random rand = new Random();
		chambres.add(new Chambre(1, rand.nextBoolean()));
		chambres.add(new Chambre(2, rand.nextBoolean()));
		chambres.add(new Chambre(3, rand.nextBoolean()));
		chambres.add(new Chambre(4, rand.nextBoolean()));
		chambres.add(new Chambre(5, rand.nextBoolean()));
		chambres.add(new Chambre(6, rand.nextBoolean()));
		chambres.add(new Chambre(7, rand.nextBoolean()));
		chambres.add(new Chambre(8, rand.nextBoolean()));
		chambres.add(new Chambre(9, rand.nextBoolean()));
		chambres.add(new Chambre(10, rand.nextBoolean()));
		chambres.add(new Chambre(11, rand.nextBoolean()));
		chambres.add(new Chambre(12, rand.nextBoolean()));
		chambres.add(new Chambre(13, rand.nextBoolean()));
		chambres.add(new Chambre(14, rand.nextBoolean()));
		chambres.add(new Chambre(15, rand.nextBoolean()));
		chambres.add(new Chambre(16, rand.nextBoolean()));
		chambres.add(new Chambre(17, rand.nextBoolean()));
		chambres.add(new Chambre(18, rand.nextBoolean()));
		chambres.add(new Chambre(19, rand.nextBoolean()));
		chambres.add(new Chambre(20, rand.nextBoolean()));

		Hotel monHotel = new Hotel(nomHotel, nombreDeChambre, Hotel.chambres);

		System.out.println(monHotel);
		affichage();
	}

}
