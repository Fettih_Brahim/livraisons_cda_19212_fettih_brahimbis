package fr.afpa.hotel.fettih;



public class Chambre {

	private boolean disponible;
	private int numeroDeChambre;


	public Chambre() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Chambre(int numeroDeChambre, boolean disponible) {
		super();
		this.numeroDeChambre = numeroDeChambre;
		Chambre.this.disponible = disponible;
	}

	
	public boolean isDisponible() {

		return disponible;
	}

	public void setDisponible(boolean disponible) {
		this.disponible = disponible;
	}

	public int getNumeroDeChambre() {
		return numeroDeChambre;
	}

	public void setNumeroDeChambre(int numeroDeChambre) {
		this.numeroDeChambre = numeroDeChambre;
	}

		
	@Override
	public String toString() {
		return "Chambre n�" + numeroDeChambre + "," + " " + "disponible=" + isDisponible();
	}
	

	
}


